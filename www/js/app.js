angular.module('starter', ['ionic', , 'ionic-timepicker'])

.run(function($ionicPlatform) {
    $ionicPlatform.ready(function() {
      if (window.cordova && window.cordova.plugins.Keyboard) {

        cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);

        cordova.plugins.Keyboard.disableScroll(true);
      }
      if (window.StatusBar) {
        StatusBar.styleDefault();
      }
    });
  })
  .config(function(ionicTimePickerProvider) {
    var timePickerObj = {
      inputTime: (((new Date()).getHours() * 60 * 60) + ((new Date()).getMinutes() * 60)),
      format: 24,
      step: 10,
      setLabel: 'Festlegen',
      closeLabel: 'Abbrechen'
    };
    ionicTimePickerProvider.configTimePicker(timePickerObj);
  })

.service('TimeService', function() {


})

.controller('HomeCtrl', function($scope) {


  })
.controller('CalcWannUpCtrl', function($scope) {
    $scope.myValue = null;
    $scope.$on('my_event', function(event, data) {
      $scope.myValue = data.timeStamp;
      console.log($scope.myValue);
    });
  })

.controller('WannCtrl', function($scope, ionicTimePicker, $state) {
  var ipObj1 = {
    callback: function(val) { //Mandatory
      if (typeof(val) === 'undefined') {
        console.log('Time not selected');
      } else {
        var selectedTime = new Date(val * 1000);
        console.log(selectedTime);
        $state.go("calcwakeup");
        $scope.$broadcast('my_event', {
          timeStamp: selectedTime
        });

        console.log('Selected epoch is : ', val, 'and the time is ', selectedTime.getHours(), 'H :', selectedTime.getMinutes(), 'M');

      }
    },
    inputTime: 80100, //Optional
    format: 24, //Optional
    step: 10, //Optional
    setLabel: 'Festlegen' //Optional
  };
  $scope.reopen = function() {
    ionicTimePicker.openTimePicker(ipObj1);
  }
  ionicTimePicker.openTimePicker(ipObj1);

})

.config(function($stateProvider, $urlRouterProvider, $ionicConfigProvider) {

  $ionicConfigProvider.views.swipeBackEnabled(false);

  $stateProvider
    .state('home', {
      url: '/home',
      templateUrl: 'templates/home.html',
      controller: 'HomeCtrl'
    })
    .state('wanns', {
      url: '/wanns',
      templateUrl: 'templates/wannschlafen.html',
      controller: 'WannCtrl'
    })
    .state('calcwakeup', {
      url: '/calcwakeup',
      templateUrl: 'templates/calc-wakeup.html',
      controller: 'CalcWannUpCtrl'
    })
  $urlRouterProvider.otherwise('/home');
});
